//
//  AppDelegate.h
//  NoSleep
//
//  Created by Petros Loukareas on 11/08/2020.
//  Copyright © 2020 Petros Loukareas. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <ServiceManagement/ServiceManagement.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

- (void)openPanelOK;
- (void)openPanelCancel;

@end

