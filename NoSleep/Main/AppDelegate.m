//
//  AppDelegate.m
//  NoSleep
//
//  Created by Petros Loukareas on 11/08/2020.
//  Copyright © 2020 Petros Loukareas. All rights reserved.
//

#import "AppDelegate.h"
#import "NOUtilities.h"
#import "NOApplication.h"
#import "NOOpenPanel.h"

#import <IOKit/pwr_mgt/IOPMLib.h>


@interface AppDelegate () <NSTableViewDelegate, NSTableViewDataSource, NSOpenSavePanelDelegate, NSMenuDelegate>

@property __unsafe_unretained IBOutlet NSWindow *window;
@property __unsafe_unretained IBOutlet NSTableView *tableView;
@property __unsafe_unretained IBOutlet NSPopUpButton *popUpButton;
@property __unsafe_unretained IBOutlet NSMenu *menu;
@property __unsafe_unretained IBOutlet NSMenuItem *isInAutomaticModeMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *isActivatedManuallyMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *isCompletelyDisabledMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *chooseFromListSeparator;
@property __unsafe_unretained IBOutlet NSMenuItem *chooseFromListMenuItem;
@property __unsafe_unretained IBOutlet NSButton *removeButton;
@property __unsafe_unretained IBOutlet NSButton *startAtLoginCheckBox;
@property __unsafe_unretained IBOutlet NSButton *showAddableItemsCheckBox;
@property __unsafe_unretained IBOutlet NSButton *showAppSwitcherCheckBox;
@property __unsafe_unretained IBOutlet NSMenuItem *statusMenuItem;
@property __unsafe_unretained IBOutlet NSView *statusView;
@property __unsafe_unretained IBOutlet NSTextField *statusMessage;
@property __unsafe_unretained IBOutlet NSBox *statusDot;

@property (strong) NSStatusItem *statusItem;
@property (strong) NSMutableArray <NOApplication *> *applicationList;
@property (strong) NSMutableArray <NOApplication *> *runningApps;
@property (strong) NSNotificationCenter *notificationCenter;
@property (strong) NSOpenPanel *filePanel;

@property (assign) NSInteger openAppCount;
@property (assign) BOOL noSleepIsActive;
@property (assign) BOOL collectThreadIsBlocked;
@property (assign) BOOL slatedToRunAgain;
@property (assign) BOOL firstOpen;
@property (assign) BOOL loadingPrefs;

@property (assign) IOPMAssertionID assertionID;

@property (assign) BOOL noSleepManuallyEnabled;
@property (assign) BOOL noSleepCompletelyDisabled;
@property (assign) BOOL startAtLogin;
@property (assign) BOOL showAddableAppsInMenu;
@property (assign) BOOL showAppSwitcher;

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    NSArray *alreadyRunning = [NSRunningApplication runningApplicationsWithBundleIdentifier:@"nl.kinoko-house.NoSleep"];
    if ([alreadyRunning count] > 1) [NSApp terminate:nil];
    _firstOpen = YES;
    _loadingPrefs = NO;
    [self loadPrefs];
    [_statusMenuItem setView:_statusView];
    [_statusMenuItem setEnabled:YES];
    [_tableView registerForDraggedTypes:[NSArray arrayWithObject:NSFilenamesPboardType]];
    _assertionID = 0;
    _collectThreadIsBlocked = NO;
    _notificationCenter = [[NSWorkspace sharedWorkspace] notificationCenter];
    [_tableView deselectAll:nil];
    [self loadAppList];
    [_tableView reloadData];
    [self installStatusItem];
    [_statusDot setNeedsDisplay:YES];
    [self compileMenus];
    [self installObservers];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    [self savePrefs];
    [self reenableScreenSleep];
    [self saveAppList];
    [self removeObservers];
    [self discardStatusItem];
}


#pragma mark Application List Gathering and Filtering

- (NSMutableArray <NSRunningApplication *> *)getRunningApplications {
    __block NSMutableArray <NSRunningApplication *> *runningApps;
    NSCondition *waitHandle = [NSCondition new];
    [waitHandle lock];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        runningApps = [[[NSWorkspace sharedWorkspace] runningApplications] mutableCopy];
        for (NSUInteger i = [runningApps count]; i > 0; i--) {
            NSRunningApplication *runningApp = [runningApps objectAtIndex:i - 1];
            if ([runningApp activationPolicy] != NSApplicationActivationPolicyRegular) {
                [runningApps removeObjectAtIndex:i - 1];
            }
        }
        [waitHandle signal];
    });
    [waitHandle waitUntilDate:[NSDate dateWithTimeIntervalSinceNow:60]];
    return runningApps;
}

- (NSMutableArray <NOApplication *> *)getAppObjectListFromArray:(NSMutableArray <NSRunningApplication *> *)applist {
    NSMutableArray *appArray = [[NSMutableArray alloc] init];
    for (NSRunningApplication *app in applist) {
        NSWorkspace *ws = [NSWorkspace sharedWorkspace];
        NSString *name = [app localizedName];
        NSString *bundleId = [app bundleIdentifier];
        NSString *path = [ws absolutePathForAppBundleWithIdentifier:bundleId];
        if (path != nil) {
            NOApplication *application = [[NOApplication alloc] initWithName:name bundleId:bundleId uuid:[NOUtilities createUUID] path:path];
            [appArray addObject:application];
        }
    }
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    [appArray sortUsingDescriptors:@[sortDescriptor]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bundleId != %@", @"nl.kinoko-house.NoSleep"];
    appArray = [[appArray filteredArrayUsingPredicate:predicate] mutableCopy];
    return [appArray mutableCopy];
}

- (NSMutableArray <NOApplication *> *)filterOutAppsAlreadyInList:(NSMutableArray <NOApplication *> *)applist {
    NSPredicate *predicate;
    for (NSInteger k = 0; k < [_applicationList count]; k++) {
        NSString *thisBundleId = [[_applicationList objectAtIndex:k] bundleId];
        predicate = [NSPredicate predicateWithFormat:@"bundleId != %@", thisBundleId];
        applist = [[applist filteredArrayUsingPredicate:predicate] mutableCopy];
    }
    predicate = [NSPredicate predicateWithFormat:@"bundleId != %@", @"nl.kinoko-house.NoSleep"];
    applist = [[applist filteredArrayUsingPredicate:predicate] mutableCopy];
    return applist;
}

- (void)compileMenus {
    if (_collectThreadIsBlocked) {
        if (_slatedToRunAgain) return;
        [self runAgainAfterSeconds:2.0f];
        _slatedToRunAgain = YES;
        return;
    }
    _collectThreadIsBlocked = YES;
    NSWorkspace *ws = [NSWorkspace sharedWorkspace];
    NSMutableArray <NSRunningApplication *> *runningAppList;
    NSMutableArray <NOApplication *> *appList;
    runningAppList = [self getRunningApplications];
    appList = [self getAppObjectListFromArray:runningAppList];
    _runningApps = [self filterOutAppsAlreadyInList:appList];
    _openAppCount = [appList count] - [_runningApps count];
    [_chooseFromListMenuItem setTitle:@"Building application list..."];
    [_popUpButton setEnabled:NO];
    [self emptyMenus];
    [self hideOrShowChooseMenu];
    NSMenuItem *pauseItem = [[NSMenuItem alloc] initWithTitle:@"Building application menu..." action:nil keyEquivalent:@""];
    [[_popUpButton menu] addItem:pauseItem];
    for (NOApplication *app in _runningApps) {
        NSMenuItem *menuItem = [[NSMenuItem alloc] initWithTitle:[app name] action:@selector(addThisApplication:) keyEquivalent:@""];
        NSImage *icon = [ws iconForFile:[app path]];
        [icon setSize:NSMakeSize(16.0f, 16.0f)];
        [menuItem setImage:icon];
        NSUInteger number = [_runningApps indexOfObject:app];
        [menuItem setRepresentedObject:[NSString stringWithFormat:@"%lu", number]];
        NSMenuItem *newMenuItem = [menuItem copy];
        if (_showAddableAppsInMenu) {
            [_menu addItem:menuItem];
        }
        [[_popUpButton menu] addItem:newMenuItem];
    }
    [[_popUpButton menu] removeItemAtIndex:0];
    [_popUpButton setEnabled:YES];
    [_chooseFromListMenuItem setTitle:@"Add application to list:"];
    if (_showAppSwitcher) {
        NSMutableArray *switchableApps = [[NSMutableArray alloc] init];
        for (NOApplication *app in _applicationList) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bundleId == %@", [app bundleId]];
            NSArray *array = [appList filteredArrayUsingPredicate:predicate];
            if ([array count] > 0) {
                [switchableApps addObject:app];
            }
        }
        if ([switchableApps count] > 0) {
            NSWorkspace *ws = [NSWorkspace sharedWorkspace];
            NSMenuItem *sep = [NSMenuItem separatorItem];
            [_menu addItem:sep];
            NSMenuItem *switchheader = [[NSMenuItem alloc] initWithTitle:@"Running applications in list:" action:nil keyEquivalent:@""];
            [switchheader setEnabled:NO];
            [_menu addItem:switchheader];
            for (NOApplication *app in switchableApps) {
                NSMenuItem *item = [[NSMenuItem alloc] initWithTitle:[app name] action:@selector(activateAppFromAppSwitcher:) keyEquivalent:@""];
                [item setIdentifier:[app bundleId]];
                NSImage *icon = [ws iconForFile:[app path]];
                [icon setSize:NSMakeSize(16.0f, 16.0f)];
                [item setImage:icon];
                [_menu addItem:item];
            }
        }
    }
    if (_openAppCount > 0 || _noSleepManuallyEnabled) {
        if (!_noSleepCompletelyDisabled) {
            if (!_noSleepIsActive) {
                [self disableScreenSleep];
            }
        } else {
            [self reenableScreenSleep];
        }
    } else {
        if (_noSleepIsActive) {
            if (!_noSleepManuallyEnabled) {
                [self reenableScreenSleep];
            }
        }
    }
    [self figureOutAppStatus];
    _collectThreadIsBlocked = NO;
}

- (void)emptyMenus {
    while ([[_menu itemArray] count] > 8) {
        [_menu removeItemAtIndex:8];
    }
    [[_popUpButton menu] removeAllItems];
}

- (void)hideOrShowChooseMenu {
    if (_showAddableAppsInMenu) {
        [_chooseFromListSeparator setHidden:NO];
        [_chooseFromListMenuItem setHidden:NO];
    } else {
        [_chooseFromListSeparator setHidden:YES];
        [_chooseFromListMenuItem setHidden:YES];
    }
}

- (void)runAgainAfterSeconds:(double)seconds {
    [self performSelector:@selector(runAgain) withObject:nil afterDelay:seconds];
}

- (void)runAgain {
    _slatedToRunAgain = NO;
    [self compileMenus];
}

- (void)activateAppFromAppSwitcher:(id)obj {
    NSString *bundleId = [obj identifier];
    NSString *command = [NSString stringWithFormat:@"tell application id \"%@\" to activate\n", bundleId];
    NSAppleScript *com = [[NSAppleScript alloc] initWithSource:command];
    [com executeAndReturnError:nil];
}

- (void)figureOutAppStatus {
    if (_noSleepManuallyEnabled) {
        [_statusMessage setStringValue:@"Status: Force Enabled"];
        [_statusDot setFillColor:[NSColor greenColor]];
        [_isActivatedManuallyMenuItem setState:NSControlStateValueOn];
        [_isCompletelyDisabledMenuItem setState:NSControlStateValueOff];
        [_isInAutomaticModeMenuItem setState:NSControlStateValueOff];
        [self setOnStatusItem];
    } else if (_noSleepCompletelyDisabled) {
        [_statusMessage setStringValue:@"Status: Disabled"];
        [_statusDot setFillColor:[NSColor redColor]];
        [_isActivatedManuallyMenuItem setState:NSControlStateValueOff];
        [_isCompletelyDisabledMenuItem setState:NSControlStateValueOn];
        [_isInAutomaticModeMenuItem setState:NSControlStateValueOff];
        [self setOffStatusItem];
    } else {
        [_statusDot setFillColor:[self getAutomaticDotColor]];
        [self setAutomaticStatus];
        [_isActivatedManuallyMenuItem setState:NSControlStateValueOff];
        [_isCompletelyDisabledMenuItem setState:NSControlStateValueOff];
        [_isInAutomaticModeMenuItem setState:NSControlStateValueOn];
        if (_noSleepIsActive) {
            [self setOnStatusItem];
        } else {
            [self setOffStatusItem];
        }
    }
}

- (NSColor *)getAutomaticDotColor {
    if (_noSleepIsActive) {
        return [NSColor cyanColor];
    } else {
        return [NSColor grayColor];
    }
}

- (void)setAutomaticStatus {
    if ([[_statusDot fillColor] isEqualTo:[NSColor grayColor]]) {
        [_statusMessage setStringValue:@"Status: Auto (inactive)"];
    } else if ([[_statusDot fillColor] isEqualTo:[NSColor cyanColor]]) {
        [_statusMessage setStringValue:@"Status: Auto (active)"];
    }
}

- (NSColor *)getDotColor {
    if (_noSleepManuallyEnabled) {
        return [NSColor greenColor];
    } else if (_noSleepCompletelyDisabled) {
        return [NSColor redColor];
    } else {
        return [self getAutomaticDotColor];
    }
}

- (void)addThisApplication:(id)obj {
    int selection = [[obj representedObject] intValue];
    NOApplication *selectedApp = [_runningApps objectAtIndex:selection];
    [_applicationList addObject:selectedApp];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    [_applicationList sortUsingDescriptors:@[sortDescriptor]];
    _applicationList = [_applicationList mutableCopy];
    [_tableView reloadData];
    [_runningApps removeObject:selectedApp];
    [self compileMenus];
}

- (NSMutableArray *)addAppsFromURLList:(NSArray *)urls {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSMutableArray *uuids = [[NSMutableArray alloc] init];
    for (NSURL *url in urls) {
        NSString *path = [url path];
        if (path != nil) {
            NSString *appname = [path lastPathComponent];
            NSString *name = [fm displayNameAtPath:path];
            NSString *uuid = [NOUtilities createUUID];
            [uuids addObject:uuid];
            NSString *bundleId = [NOUtilities bundleIdentifierForApplicationName:appname];
            NOApplication *addedApp = [[NOApplication alloc] initWithName:name bundleId:bundleId uuid:uuid path:path];
            [_applicationList addObject:addedApp];
        }
    }
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    [_applicationList sortUsingDescriptors:@[sortDescriptor]];
    _applicationList = [_applicationList mutableCopy];
    return uuids;
}


#pragma mark Status Item

- (void)installStatusItem {
    if (_statusItem == nil) {
        NSStatusBar *statusBar = [NSStatusBar systemStatusBar];
        _statusItem = [statusBar statusItemWithLength:24];
        if (@available(macOS 10.12, *)) {
            [_statusItem setAutosaveName:@"NoSleepStatusItem"];
            [_statusItem setVisible:YES];
        }
        [self setOffStatusItem];
        [_statusItem setMenu:_menu];
    }
}

- (void)discardStatusItem {
    if (_statusItem) {
        [[NSStatusBar systemStatusBar] removeStatusItem: _statusItem];
        _statusItem = nil;
    }
}

- (void)setOnStatusItem {
    NSImage *image = [NSImage imageNamed:@"statusitem-on"];
    [image setSize:NSMakeSize(16, 16)];
    [image setTemplate:YES];
    [_statusItem setImage:image];
}

- (void)setOffStatusItem {
    NSImage *image = [NSImage imageNamed:@"statusitem-off"];
    [image setSize:NSMakeSize(16, 16)];
    [image setTemplate:YES];
    [_statusItem setImage:image];
}


#pragma mark Sleep and Wake Functions

- (void)disableScreenSleep {
    if (_assertionID != 0) return;
    CFStringRef reasonForActivity = CFSTR("No Sleep 'till Brooklyn!");
    IOReturn success = IOPMAssertionCreateWithName(kIOPMAssertionTypePreventUserIdleDisplaySleep, kIOPMAssertionLevelOn, reasonForActivity, &_assertionID);
    if (success == kIOReturnSuccess) {
        _noSleepIsActive = YES;
        [self setOnStatusItem];
        if (!_noSleepManuallyEnabled && !_noSleepCompletelyDisabled) {
            [_statusDot setFillColor:[self getAutomaticDotColor]];
            [self setAutomaticStatus];
        }
    }
}

- (void)reenableScreenSleep {
    IOReturn success;
    if (_assertionID == 0) return;
    if (_noSleepIsActive || _noSleepManuallyEnabled) {
        do {
            success = IOPMAssertionRelease(_assertionID);
        } while (!success);
        _noSleepIsActive = NO;
        _assertionID = 0;
        if (_noSleepManuallyEnabled) {
            _noSleepManuallyEnabled = NO;
        }
    }
    [self setOffStatusItem];
    if (!_noSleepManuallyEnabled && !_noSleepCompletelyDisabled) {
        [_statusDot setFillColor:[self getAutomaticDotColor]];
        [self setAutomaticStatus];
    }
}


#pragma mark Application Notifications

- (void)installObservers {
    [_notificationCenter addObserver:self selector:@selector(appWasLaunched:) name:NSWorkspaceDidLaunchApplicationNotification object:nil];
    [_notificationCenter addObserver:self selector:@selector(appWasTerminated:) name:NSWorkspaceDidTerminateApplicationNotification object:nil];
}

- (void)removeObservers {
    [_notificationCenter removeObserver:self name:NSWorkspaceDidLaunchApplicationNotification object:nil];
    [_notificationCenter removeObserver:self name:NSWorkspaceDidTerminateApplicationNotification object:nil];
}

- (void)appWasLaunched:(NSNotification *)notification {
    [self compileMenus];
}

- (void)appWasTerminated:(NSNotification *)notification {
    [self compileMenus];
}


#pragma mark IBActions

- (IBAction)activateNoSleepManually:(id)sender {
    if (!_noSleepManuallyEnabled) {
        if (!_noSleepIsActive) {
            [self disableScreenSleep];
            _noSleepIsActive = YES;
        }
        _noSleepManuallyEnabled = YES;
        _noSleepCompletelyDisabled = NO;
        [self figureOutAppStatus];
        [self savePrefs];
    } else {
        [self setToAutomaticMode:self];
        if (_openAppCount == 0) {
            [self reenableScreenSleep];
            _noSleepIsActive = NO;
        }
    }
}

- (IBAction)disableNoSleepCompletely:(id)sender {
    if (!_noSleepCompletelyDisabled) {
        [self reenableScreenSleep];
        _noSleepIsActive = NO;
        _noSleepManuallyEnabled = NO;
        _noSleepCompletelyDisabled = YES;
        [self figureOutAppStatus];
        [self savePrefs];
    } else {
        [self setToAutomaticMode:self];
    }
}

- (IBAction)setToAutomaticMode:(id)sender {
    if (_openAppCount > 0) {
        [self disableScreenSleep];
        _noSleepIsActive = YES;
    } else {
        [self reenableScreenSleep];
        _noSleepIsActive = NO;
    }
    _noSleepManuallyEnabled = NO;
    _noSleepCompletelyDisabled = NO;
    [self figureOutAppStatus];
    [self savePrefs];
}

- (IBAction)showSettingsWindow:(id)sender {
    [_window makeKeyAndOrderFront:nil];
    [NSApp activateIgnoringOtherApps:YES];
}

- (IBAction)plusButtonClicked:(id)sender {
    [self plusButtonClicked];
}

- (IBAction)minusButtonClicked:(id)sender {
    if ([[_tableView selectedRowIndexes] count] < 1) return;
    [_applicationList removeObjectsAtIndexes:[_tableView selectedRowIndexes]];
    [self saveAppList];
    [_tableView reloadData];
    [_removeButton setEnabled:NO];
    [self compileMenus];
}

- (IBAction)startAtLoginCheckBoxClicked:(id)sender {
    NSControlStateValue state = [_startAtLoginCheckBox state];
    if (state == NSControlStateValueOn) {
        _startAtLogin = YES;
    } else {
        _startAtLogin = NO;
    }
    [self addToStartupItems:_startAtLogin];
    [self savePrefs];
}

- (IBAction)showAddableAppsCheckBoxClicked:(id)sender {
    NSControlStateValue state = [_showAddableItemsCheckBox state];
    if (state == NSControlStateValueOn) {
        _showAddableAppsInMenu = YES;
    } else {
        _showAddableAppsInMenu = NO;
    }
    [self compileMenus];
    [self savePrefs];
}

- (IBAction)showAppSwitcherCheckBoxClicked:(id)sender {
    NSControlStateValue state = [_showAppSwitcherCheckBox state];
    if (state == NSControlStateValueOn) {
        _showAppSwitcher = YES;
    } else {
        _showAppSwitcher = NO;
    }
    [self compileMenus];
    [self savePrefs];
}


#pragma mark Startup Items

- (BOOL)addToStartupItems:(BOOL)addOrNotFlag {
    return (SMLoginItemSetEnabled((__bridge CFStringRef)@"nl.kinoko-house.NoSleep-Helper", addOrNotFlag));
}


#pragma mark Storage Support

- (void)prepareAppSupportFolder {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    BOOL isDir;
    NSURL *appSupportURL = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:&error];
    if (error) {
        NSString *errorDescription = [NSString stringWithFormat:@"An error occurred while accessing the application support directory. Details in the system language are below:\n\n%li: %@\n\nPlease contact the developer if the problem persists. The application will close after you press the OK button. We are very sorry for the inconvenience.", [error code],[error localizedDescription]];
        [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
        [NOUtilities showAlertWithMessageText:@"Error while accessing application data" informativeText:errorDescription];
        [[NSApplication sharedApplication] terminate:nil];
    }
    NSString *pathToAppSupportDir = [appSupportURL path];
    NSString *pathToMyAppSupportDir = [pathToAppSupportDir stringByAppendingPathComponent:@"NoSleep"];
    NSURL *URLToMyAppSupportDir = [NSURL fileURLWithPath:pathToMyAppSupportDir];
    BOOL folderExists = [fileManager fileExistsAtPath:pathToMyAppSupportDir isDirectory:&isDir];
    if (folderExists && !isDir) {
        [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
        [NOUtilities showAlertWithMessageText:@"File found where a folder was expected" informativeText:@"There is a file with the name \"NoSleep\" in the \"Library/Application Support\" folder of your home folder. Please move this file to a different location and launch the application again. The application will close after you press the OK button."];
        [[NSApplication sharedApplication] terminate:nil];
    }
    if (!folderExists) {
        BOOL directoryCreated = [fileManager createDirectoryAtURL:URLToMyAppSupportDir withIntermediateDirectories:NO attributes:nil error:&error];
        if (!directoryCreated) {
            [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
            NSString *errorDescription = [NSString stringWithFormat:@"An error occurred while creating the application support directory. Details in the system language are below:\n\n%li: %@\n\nPlease contact the developer if the problem persists. The application will close after you press the OK button. We are very sorry for the inconvenience.", [error code],[error localizedDescription]];
            [NOUtilities showAlertWithMessageText:@"Error while creating application support directory" informativeText:errorDescription];
            [[NSApplication sharedApplication] terminate:nil];
        }
    }
}

- (void)saveAppList {
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *appSupportURL = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:&error];
    NSString *pathToAppSupportDir = [appSupportURL path];
    NSString *pathToMyAppSupportDir = [pathToAppSupportDir stringByAppendingPathComponent:@"NoSleep"];
    NSString *pathToMyAppSupportFile = [pathToMyAppSupportDir stringByAppendingPathComponent:@"NoSleep.applicationList"];
    NSArray *objecToSave = _applicationList;
    BOOL success = [NSKeyedArchiver archiveRootObject:objecToSave toFile:pathToMyAppSupportFile];
    if (!success) {
        [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
        [NOUtilities showAlertWithMessageText:@"Could not save bookmarks" informativeText:@"There was an unspecified problem saving the application list. The previous version of the application list should still be in order. Please try to relauch the application, or restore an earlier backup of your favorites list if the problem persists, or contact the developer for further help. We are very sorry for the inconvenience."];
    } else {
        [[NSWorkspace sharedWorkspace] setIcon:[NSImage imageNamed:@"applist"] forFile:pathToMyAppSupportFile options:0];
    }
}

- (void)loadAppList {
    id theObject;
    NSError *error;
    [self prepareAppSupportFolder];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *appSupportURL = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:&error];
    NSString *pathToAppSupportDir = [appSupportURL path];
    NSString *pathToMyAppSupportDir = [pathToAppSupportDir stringByAppendingPathComponent:@"NoSleep"];
    NSString *pathToMyAppSupportFile = [pathToMyAppSupportDir stringByAppendingPathComponent:@"NoSleep.applicationList"];
    @try {
        theObject = [NSKeyedUnarchiver unarchiveObjectWithFile:pathToMyAppSupportFile];
    }
    @catch (NSException *e) {
        theObject = nil;
    }
    @try {
        if (!theObject) {
            theObject = nil;
        }
    }
    @catch (NSException *e) {
        theObject = nil;
    }
    if (!theObject) {
        _applicationList = [[NSMutableArray alloc] init];
    } else {
        if ([theObject count] != 0) {
            if ([[theObject objectAtIndex:0] valueForKey:@"path"] == nil) {
                _applicationList = [[NSMutableArray alloc] init];
                NSWorkspace *ws = [NSWorkspace sharedWorkspace];
                for (id obj in theObject) {
                    NSString *name = [obj valueForKey:@"name"];
                    NSString *uuid = [obj valueForKey:@"uuid"];
                    NSString *bundleId = [obj valueForKey:@"bundleId"];
                    NSString *path = [ws absolutePathForAppBundleWithIdentifier:bundleId];
                    NOApplication *convertedApp = [[NOApplication alloc] initWithName:name bundleId:bundleId uuid:uuid path:path];
                    [_applicationList addObject:convertedApp];
                }
                return;
            } else {
                _applicationList = [theObject mutableCopy];
            }
        } else {
            _applicationList = [[NSMutableArray alloc] init];
        }
    }
}


#pragma mark Table View Delegate Methods

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    NSWorkspace *ws = [NSWorkspace sharedWorkspace];
    NOApplication *app = [_applicationList objectAtIndex:row];
    NSTableCellView *cellView = [tableView makeViewWithIdentifier:@"apptablecell" owner:self];
    [[cellView textField] setStringValue:[app name]];
    [[cellView imageView] setImage:[ws iconForFile:[app path]]];
    return cellView;
}

- (BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)row {
    return YES;
}

- (BOOL)tableView:(NSTableView *)tableView shouldEditTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    return NO;
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification {
    if ([[_tableView selectedRowIndexes] count] > 0) {
        [_removeButton setEnabled:YES];
    } else {
        [_removeButton setEnabled:NO];
    }
}


#pragma mark Table View Data Source Methods

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return [_applicationList count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    return [_applicationList objectAtIndex:row];
}


#pragma mark Table View Drag & Drop

- (NSDragOperation)tableView:(NSTableView *)aTableView validateDrop:(id <NSDraggingInfo>)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)operation {
    NSArray *availableFiles = nil;
    NSPasteboard *pboard = [info draggingPasteboard];
    if ([pboard availableTypeFromArray:@[NSFilenamesPboardType]]) {
        NSArray *fileNames = [pboard propertyListForType:NSFilenamesPboardType];
        availableFiles = [fileNames filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"pathExtension == %@", @"app"]];
    }
    if (!availableFiles) {
        return NSDragOperationNone;
    } else {
        return NSDragOperationCopy;
    }
}

- (BOOL)tableView:(NSTableView *)tableView acceptDrop:(id <NSDraggingInfo>)info row:(NSInteger)row dropOperation:(NSTableViewDropOperation)dropOperation {
    NSPasteboard *pboard = [info draggingPasteboard];
    if ([pboard availableTypeFromArray:@[NSFilenamesPboardType]]) {
        NSMutableArray *urls = [[NSMutableArray alloc] init];
        NSArray *fileNames = [pboard propertyListForType:NSFilenamesPboardType];
        NSArray *availableFiles = [fileNames filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"pathExtension == %@", @"app"]];
        if (!availableFiles) {
            return NO;
        }
        for (NSString *path in availableFiles) {
            [urls addObject:[NSURL fileURLWithPath:path]];
        }
        if ([urls count] > 0) {
            NSMutableArray *uuids = [self addAppsFromURLList:urls];
            [self saveAppList];
            [_tableView reloadData];
            [self reselectUUIDs:uuids];
            return YES;
        } else {
            return NO;
        }
    } else {
        return NO;
    }
}

- (void)reselectUUIDs:(NSMutableArray *)uuids {
    NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] init];
    for (int i = 0; i < [_applicationList count]; i++) {
        for (int j = 0; j < [uuids count]; j++) {
            if ([[uuids objectAtIndex:j] isEqualToString:[[_applicationList objectAtIndex:i] uuid]]) {
                [indexSet addIndex:i];
                break;
            }
        }
    }
    [_tableView selectRowIndexes:indexSet byExtendingSelection:NO];
}


#pragma mark Preferences

- (void)loadPrefs {
    if (_loadingPrefs) return;
    _loadingPrefs = YES;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *basicDefaults = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @NO,  @"noSleepManuallyEnabled",
                                   @NO,  @"noSleepCompletelyDisabled",
                                   @NO,  @"startAtLogin",
                                   @YES, @"showAddableAppsInMenu",
                                   @YES, @"showAppSwitcher",
                                   nil];
    [userDefaults registerDefaults:basicDefaults];
    _noSleepManuallyEnabled = [userDefaults boolForKey:@"noSleepManuallyEnabled"];
    if (_noSleepManuallyEnabled) {
        _noSleepManuallyEnabled = NO;
        [self activateNoSleepManually:self];
        _noSleepManuallyEnabled = YES;
        [_isActivatedManuallyMenuItem setState:NSControlStateValueOn];
        [_isCompletelyDisabledMenuItem setState:NSControlStateValueOff];
        [_isInAutomaticModeMenuItem setState:NSControlStateValueOff];
    }
    _noSleepCompletelyDisabled = [userDefaults boolForKey:@"noSleepCompletelyDisabled"];
    if (_noSleepCompletelyDisabled) {
        _noSleepCompletelyDisabled = NO;
        [self disableNoSleepCompletely:self];
        _noSleepCompletelyDisabled = YES;
        [_isActivatedManuallyMenuItem setState:NSControlStateValueOff];
        [_isCompletelyDisabledMenuItem setState:NSControlStateValueOn];
        [_isInAutomaticModeMenuItem setState:NSControlStateValueOff];
    }
    if ((!_noSleepCompletelyDisabled) && (!_noSleepManuallyEnabled)) {
        [_isInAutomaticModeMenuItem setState:NSControlStateValueOn];
    }
    _startAtLogin = [userDefaults boolForKey:@"startAtLogin"];
    if (_startAtLogin) {
        [_startAtLoginCheckBox setState:NSControlStateValueOn];
    } else {
        [_startAtLoginCheckBox setState:NSControlStateValueOff];
    }
    _showAddableAppsInMenu = [userDefaults boolForKey:@"showAddableAppsInMenu"];
    if (_showAddableAppsInMenu) {
        [_showAddableItemsCheckBox setState:NSControlStateValueOn];
    } else {
        [_showAddableItemsCheckBox setState:NSControlStateValueOff];
    }
    _showAppSwitcher = [userDefaults boolForKey:@"showAppSwitcher"];
    if (_showAppSwitcher) {
        [_showAppSwitcherCheckBox setState:NSControlStateValueOn];
    } else {
        [_showAppSwitcherCheckBox setState:NSControlStateValueOff];
    }
    _loadingPrefs = NO;
}

- (void)savePrefs {
    if (_loadingPrefs) return;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:_noSleepManuallyEnabled forKey:@"noSleepManuallyEnabled"];
    [userDefaults setBool:_noSleepCompletelyDisabled forKey:@"noSleepCompletelyDisabled"];
    [userDefaults setBool:_startAtLogin forKey:@"startAtLogin"];
    [userDefaults setBool:_showAddableAppsInMenu forKey:@"showAddableAppsInMenu"];
    [userDefaults setBool:_showAppSwitcher forKey:@"showAppSwitcher"];
    [userDefaults synchronize];
}


#pragma mark File Panel Methods

- (void)plusButtonClicked {
    _filePanel = [NOOpenPanel openPanel];
    [_filePanel setDelegate:self];
    [_filePanel setTitle:@"Please select application(s):"];
    [_filePanel setCanChooseDirectories:NO];
    [_filePanel setCanChooseFiles:YES];
    [_filePanel setDirectoryURL:[[[NSFileManager defaultManager] URLsForDirectory:NSApplicationDirectory inDomains:NSLocalDomainMask] firstObject]];
    [NSApp beginSheet:_filePanel modalForWindow:_window modalDelegate:self didEndSelector:@selector(sheetDidEnd:returnCode:contextInfo:) contextInfo:nil];
}

- (void)openPanelOK {
    [NSApp endSheet:_filePanel returnCode:NSModalResponseOK];
}

- (void)openPanelCancel {
    [NSApp endSheet:_filePanel returnCode:NSModalResponseCancel];
}

- (void)sheetDidEnd:(NSWindow *)sheet returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo {
    if (returnCode == NSModalResponseCancel || _filePanel.URLs == nil) {
        [NSApp endSheet:_filePanel];
        [_filePanel orderOut:nil];
        return;
    }
    NSMutableArray *uuids = [self addAppsFromURLList:_filePanel.URLs];
    [self saveAppList];
    [_tableView reloadData];
    [self reselectUUIDs:uuids];
    [NSApp endSheet:_filePanel];
    [_filePanel orderOut:nil];
    [self compileMenus];
}


#pragma mark File Panel Delegate Methods

- (BOOL)panel:(id)sender shouldEnableURL:(NSURL *)url {
    BOOL isApp = [[url pathExtension] isEqualToString:@"app"];
    BOOL isDir = [[[url absoluteString] substringWithRange:NSMakeRange([[url absoluteString] length] - 1, 1)] isEqualToString:@"/"];
    return (isApp || isDir);
}


#pragma mark Menu Delegate Methods

- (void)menuWillOpen:(NSMenu *)menu {
    if (_firstOpen) {
        NSTimer *timer = [NSTimer timerWithTimeInterval:0.025f target:self selector:@selector(redrawDot) userInfo:nil repeats:NO];
        [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    }
}

- (void)redrawDot {
    [_statusDot setFillColor:[NSColor blueColor]];
    [_statusDot setFillColor:[self getDotColor]];
    _firstOpen = NO;
}


@end


