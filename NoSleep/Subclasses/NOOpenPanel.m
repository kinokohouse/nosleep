//
//  NOOpenPanel.m
//  NoSleep
//
//  Created by Petros Loukareas on 12/08/2020.
//  Copyright © 2020 Petros Loukareas. All rights reserved.
//

#import "NOOpenPanel.h"
#import "AppDelegate.h"

@interface NOOpenPanel ()

@end

@implementation NOOpenPanel

- (IBAction)cancel:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [appDelegate openPanelCancel];
}

- (IBAction)ok:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [appDelegate openPanelOK];
}

@end
