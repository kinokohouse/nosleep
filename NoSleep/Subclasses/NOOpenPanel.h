//
//  NOOpenPanel.h
//  NoSleep
//
//  Created by Petros Loukareas on 12/08/2020.
//  Copyright © 2020 Petros Loukareas. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface NOOpenPanel : NSOpenPanel

@end

NS_ASSUME_NONNULL_END
