//
//  NOApplication.m
//  NoSleep
//
//  Created by Petros Loukareas on 11/08/2020.
//  Copyright © 2020 Petros Loukareas. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "NOApplication.h"
#import "NOUtilities.h"

@implementation NOApplication

- (id)init {
    if (self = [super init]) {
        [self setName:@"Application"];
        [self setUuid:[NOUtilities createUUID]];
        [self setBundleId:@"nl.kinoko-house.app"];
        [self setPath:@""];
        return self;
    } else {
        return nil;
    }
}

- (id)initWithName:(NSString *)name bundleId:(NSString *)bundleId uuid:(NSString *)uuid path:(NSString *)path {
    if (self = [super init]) {
        [self setName:name];
        [self setUuid:uuid];
        [self setBundleId:bundleId];
        [self setPath:path];
        return self;
    } else {
        return nil;
    }
}

- (id)copyWithZone:(NSZone *)zone {
    NOApplication *newApplication = [[[self class] allocWithZone:zone] init];
    if (newApplication) {
        [newApplication setName:[self name]];
        [newApplication setUuid:[self uuid]];
        [newApplication setBundleId:[self bundleId]];
        [newApplication setPath:[self path]];
        return newApplication;
    } else {
        return nil;
    }
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        [self setName:[aDecoder decodeObjectForKey:@"name"]];
        [self setUuid:[aDecoder decodeObjectForKey:@"uuid"]];
        [self setBundleId:[aDecoder decodeObjectForKey:@"bundleId"]];
        [self setPath:[aDecoder decodeObjectForKey:@"path"]];
        return self;
    } else {
        return nil;
    }
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:[self name] forKey:@"name"];
    [aCoder encodeObject:[self uuid] forKey:@"uuid"];
    [aCoder encodeObject:[self bundleId] forKey:@"bundleId"];
    [aCoder encodeObject:[self path] forKey:@"path"];
}

@end
