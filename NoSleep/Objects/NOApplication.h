//
//  NOApplication.h
//  NoSleep
//
//  Created by Petros Loukareas on 11/08/2020.
//  Copyright © 2020 Petros Loukareas. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NOApplication : NSObject <NSCoding>

@property (copy) NSString *name;
@property (copy) NSString *uuid;
@property (copy) NSString *bundleId;
@property (copy) NSString *path;

- (id)init;
- (id)initWithName:(NSString *)name bundleId:(NSString *)bundleId uuid:(NSString *)uuid path:(NSString *)path;
- (id)copyWithZone:(NSZone *)zone;
- (instancetype)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)aCoder;

@end

NS_ASSUME_NONNULL_END
