//
//  NOUtilities.m
//  NoSleep
//
//  Created by Petros Loukareas on 11/08/2020.
//  Copyright © 2020 Petros Loukareas. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "NOUtilities.h"

@implementation NOUtilities

+ (NSString *)createUUID {
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge_transfer NSString *)string;
}

+ (void)showAlertWithMessageText:(NSString *)topText informativeText:(NSString *)mainText {
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setMessageText:topText];
    [alert setInformativeText:mainText];
    [alert addButtonWithTitle:@"OK"];
    [alert setAlertStyle:NSCriticalAlertStyle];
    [alert runModal];
}

+ (NSString *)bundleIdentifierForApplicationName:(NSString *)appName {
    NSWorkspace * workspace = [NSWorkspace sharedWorkspace];
    NSString * appPath = [workspace fullPathForApplication:appName];
    if (appPath) {
        NSBundle * appBundle = [NSBundle bundleWithPath:appPath];
        return [appBundle bundleIdentifier];
    }
    return nil;
}


@end
