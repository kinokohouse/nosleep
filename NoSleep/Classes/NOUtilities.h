//
//  NOUtilities.h
//  NoSleep
//
//  Created by Petros Loukareas on 11/08/2020.
//  Copyright © 2020 Petros Loukareas. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NOUtilities : NSObject

+ (NSString *)createUUID;
+ (void)showAlertWithMessageText:(NSString *)topText informativeText:(NSString *)mainText;
+ (NSString *)bundleIdentifierForApplicationName:(NSString *)appName;

@end

NS_ASSUME_NONNULL_END
