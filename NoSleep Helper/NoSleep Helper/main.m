//
//  main.m
//  NoSleep Helper
//
//  Created by Petros Loukareas on 12/08/2020.
//  Copyright © 2020 Petros Loukareas. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
