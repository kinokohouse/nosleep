//
//  NHAppDelegate.h
//  NoSleep Helper
//
//  Created by Petros Loukareas on 12/08/2020.
//  Copyright © 2020 Petros Loukareas. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NHAppDelegate : NSObject <NSApplicationDelegate>


@end

