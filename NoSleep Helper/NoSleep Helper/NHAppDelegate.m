//
//  NHAppDelegate.m
//  NoSleep Helper
//
//  Created by Petros Loukareas on 12/08/2020.
//  Copyright © 2020 Petros Loukareas. All rights reserved.
//

#import "NHAppDelegate.h"

@implementation NHAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    NSString *path = [[[[[[NSBundle mainBundle] bundlePath] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent];
    [[NSWorkspace sharedWorkspace] launchApplication:path];
    [NSApp terminate:nil];
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Nothing to do
}


@end
