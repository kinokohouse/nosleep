# NoSleep #
All I wanted is to play The Binding of Isaac without the screen saver kicking in.

There are a number of utilities to keep your Mac from sleeping when you'd rather have it wouldn't. Some of those are based on simply blocking sleep altogether, and other can check if a certain application or process is running to see if your machine should sleep or not. But in some cases, you just want to prevent screen sleep if one or more specific applications are up and running (case in point, to get back to Isaac: don't sleep when the Steam client is running, which for some reason, despite being a *gaming* client, does not appear to be able to prevent screen sleep).

This simple app will do this for you. Just add the offending apps to the list, set the mode to `automatic` and off you go; the regular screen sleep cycle will resume after the listed app(s) has/have terminated. You can also force non-sleep or disable the app altogether to ignore any open apps in the list.

Requires 10.7+. Ready to use binary (unsigned, so launch it with right-click-open when running it for the first time) from the `Downloads` section. MIT License.

Icons from [FlatIcon](https://www.flaticon.com); artist is [Freepik](https://www.freepik.com).

## Building ##

You'll need Xcode 10 to build this yourself.


### Version History

**1.12 (build 4, current version)**

* Clarified the automatic status in the status menu for a bit - both in wording...
* ...and in coloring (`gray` for inactive, `cyan` for active - as opposed to `red` for force disabled and `green` for force enabled.

**1.11 (build 3)**

* I really wish that Xcode would warn you if you try to use weak references in something that should run on 10.7... My humble apologies, and a new binary is of course also available.
* Also, the icons were ugly, so I fixed them.

**1.1 (build 2)**

* Added a proper way to add the app to your login items;
* Added a choice to switch off the showing of running apps in the status menu;
* Added an option to show a separate list in the of running apps that appear in the list (which can also be used to switch to that app);
* The app status is now not clickable anymore;
* A new file format that does not actually include the icons (so no more 50MB application lists with two entries). This does however mean that the old format is now invalid, but the application will transparently convert the application list to the new format.


**1.0 (build 1)**

* Initial release.
